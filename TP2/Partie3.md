# Partie 3 : Maintien en condition opérationnelle


## Sommaire

- [Partie 3 : Maintien en condition opérationnelle](#partie-3--maintien-en-condition-opérationnelle)
  - [Sommaire](#sommaire)
- [I. Monitoring](#i-monitoring)
  - [1. Intro](#1-intro)
  - [2. Setup Netdata](#2-setup-netdata)
  - [3. Bonus : alerting](#3-bonus--alerting)
  - [4. Bonus : proxying](#4-bonus--proxying)
- [II. Backup](#ii-backup)

# I. Monitoring

## 1. Intro

🌞 **Installez Netdata** en exécutant la commande suivante :

```bash
[admin@proxy nginx]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
```


🌞 **Démarrez Netdata**

- vous avez un service `netdata`
```
[admin@proxy nginx]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 15:36:14 CET; 1min 29s ago
   ```

- ouvrez le port dans le firewall 19999/tcp : c'est l'interface Web de Netdata (vous pouvez le vérifier avec une commande `ss` comme toujours !)

```
[admin@proxy nginx]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success
[admin@proxy nginx]$ sudo firewall-cmd --reload
success
```

Bah voilà hein ! Ouvrez votre navigateur, go `http://<IP_VM>:19999` and enjoy ==> connexion ok


## 3. Bonus : alerting

🌞 **Mettez en place un alerting Discord**

- [lien de la doc](https://learn.netdata.cloud/docs/agent/health/notifications/discord) pour l'alerting Discord
- le principe est de recevoir les alertes dans un salon texte dédié sur un serveur Discord

## 4. Bonus : proxying

Euh bah là on a Apache protégé par NGINX, mais le serveur Web à moitié naze que Netdata utilise, pas de soucis ? BAH SI c'est un soucis :'(

🌞 **Ajustez la conf**

- l'interface web de Netdata doit être joignable par le reverse proxy

# II. Backup


🌞 **Téléchargez et jouez avec Borg** sur la machine `web.tp2.cesi`

```
[admin@web ~]$ sudo curl -SLO https://github.com/borgbackup/borg/releases/download/1.1.17/bo
rg-linux64
[sudo] password for admin:
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   649  100   649    0     0   2351      0 --:--:-- --:--:-- --:--:--  2351
100 18.0M  100 18.0M    0     0  4419k      0  0:00:04  0:00:04 --:--:-- 5012k
[admin@web ~]$ sudo cp borg-linux64 /usr/local/bin/borg
[admin@web ~]$ sudo chown root:root /usr/local/bin/borg
[admin@web ~]$ sudo chmod 755 /usr/local/bin/borg
```

- créez un dépôt, faites des sauvegardes, restaures des sauvegardes, testez l'outil un peu
```
[admin@web ~]$ sudo /usr/local/bin/borg init -e repokey /srv/backups/
Enter new passphrase:
Enter same passphrase again:
Do you want your passphrase to be displayed for verification? [yN]: n

By default repositories initialized with this version will produce security
errors if written to with an older version (up to and including Borg 1.0.8).

If you want to use these older versions, you can disable the check by running:
borg upgrade --disable-tam /srv/backups

See https://borgbackup.readthedocs.io/en/stable/changes.html#pre-1-0-9-manifest-spoofing-vulnerability for details about the security implications.

IMPORTANT: you will need both KEY AND PASSPHRASE to access this repo!
If you used a repokey mode, the key is stored in the repo, but you should back it up separately.
Use "borg key export" to export the key, optionally in printable format.
Write down the passphrase. Store both at safe place(s).

# création du backup
[admin@web backups]$ sudo /usr/local/bin/borg create /srv/backups::Wednesday8 ~/backups

# Test du backup
[admin@web backups]$ sudo /usr/local/bin/borg create -v --stats /srv/backups::Thursday9 ~/backups
Creating archive at "/srv/backups::Thursday9"
/home/admin/backups: [Errno 2] No such file or directory: '/home/admin/backups'
------------------------------------------------------------------------------
Archive name: Thursday9
Archive fingerprint: 59febd309319263a294885f386196b3e0f22abacab22f7ec6100b1da1491b26e
Time (start): Wed, 2021-12-08 04:27:57
Time (end):   Wed, 2021-12-08 04:27:57
Duration: 0.00 seconds
Number of files: 0
Utilization of max. archive size: 0%
------------------------------------------------------------------------------
                       Original size      Compressed size    Deduplicated size
This archive:                  420 B                442 B                442 B
All archives:                  833 B                883 B                883 B

                       Unique chunks         Total chunks
Chunk index:                       2                    2
------------------------------------------------------------------------------

# lister les backups
[admin@web backups]$ sudo /usr/local/bin/borg list /srv/backups
[sudo] password for admin:
Wednesday8                           Wed, 2021-12-08 04:16:07 [917da88342481aa5a98b7af9e74087ed30fc9d7fa607d54666226bc8f81d0429]
Thursday9                            Wed, 2021-12-08 04:27:57 [59febd309319263a294885f386196b3e0f22abacab22f7ec6100b1da1491b26e]

# restaurer une sauvegarde
[admin@web backups]$ sudo /usr/local/bin/borg extract /srv/backups::Wednesday8

```

🌞 **Ecrire un script**

- il doit sauvegarder le dossier où se trouve NextCloud
- le dépôt borg doit être dans le dossier `/srv/backup/`
- le nom de la sauvegarde doit être `nextcloud_YYMMDD_HHMMSS`
- testez le à la main, avant de continuer

🌞 **Créer un service**

- créer un service `backup_db.service` qui exécute votre script
- ainsi, quand on lance le service, une backup de la base de données est déclenchée

La syntaxe, toujours la même :

```bash
[Unit]
Description=<DESCRIPTION>

[Service]
ExecStart=<COMMAND>
Type=oneshot

[Install]
WantedBy=multi-user.target
```

**NB : vous DEVEZ ajoutez la ligne `Type=oneshot` en dessous de la ligne `ExecStart=` dans votre service**. Cela indique au système que ce service ne sera pas un démon qui s'exécute en permanence, mais un script s'eécutera, puis qui aura une fin d'exécution (sinon le système peut par exemple essayer de relancer automatiquement un service qui s'arrête).

🌞 **Créer un timer**

- un *timer* c'est un fichier qui permet d'exécuter un service à intervalles réguliers
- créez un *timer* qui exécute le service `backup` toutes les heures

Pour cela, créer le fichier `/etc/systemd/system/backup.timer`.

> Notez qu'il est dans le même dossier que le service, et qu'il porte le même nom, mais pas la même extension.

Contenu du fichier `/etc/systemd/system/backup.timer` :

```bash
[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup.service

[Timer]
Unit=backup.service
OnCalendar=hourly

[Install]
WantedBy=timers.target
```

> Au niveau du `OnCalendar`, on précise tout un tas de trucs : une heure précise une fois par semaine, toutes les trois heures les jours pairs (ui c'est improbable, mais on peut !), etc.

Activez maintenant le *timer* avec :

```bash
# on indique qu'on a modifié la conf du système
$ sudo systemctl daemon-reload

# démarrage immédiat du timer
$ sudo systemctl start backup.timer

# activation automatique du timer au boot de la machine
$ sudo systemctl enable backup.timer
```

🌞 **Vérifier que le *timer* a été pris en compte**, en affichant l'heure de sa prochaine exécution :

```bash
$ sudo systemctl list-timers
```

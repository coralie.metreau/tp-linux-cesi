## Sommaire

- [Partie 1 : Mise en place de la solution](#partie-1--mise-en-place-de-la-solution)
  - [Sommaire](#sommaire)
- [I. Setup base de données](#i-setup-base-de-données)
  - [1. Install MariaDB](#1-install-mariadb)
  - [2. Conf MariaDB](#2-conf-mariadb)
  - [3. Test](#3-test)
- [II. Setup Apache](#ii-setup-apache)
  - [1. Install Apache](#1-install-apache)
    - [A. Apache](#a-apache)
    - [B. PHP](#b-php)
  - [2. Conf Apache](#2-conf-apache)
- [III. NextCloud](#iii-nextcloud)
  - [4. Test](#4-test)

# I. Setup base de données

## 1. Install MariaDB

🌞 **Installer MariaDB sur la machine `db.tp2.cesi`**

```
[admin@db ~]$ sudo dnf install mariadb-server
Installed:
  mariadb-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                 mariadb-backup-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-common-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64          mariadb-connector-c-3.1.11-2.el8_3.x86_64
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch                       mariadb-errmsg-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-gssapi-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64   mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-server-utils-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64    perl-DBD-MySQL-4.046-3.module+el8.4.0+577+b8fe2d92.x86_64
  perl-DBI-1.641-3.module+el8.4.0+509+59a8d9b3.x86_64                    perl-Math-BigInt-1:1.9998.11-7.el8.noarch
  perl-Math-Complex-1.59-420.el8.noarch                                  psmisc-23.1-5.el8.x86_64

Complete!
```

🌞 **Le service MariaDB**

- le service s'appelle `mariadb`
```
[admin@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
```

- lancez-le avec une commande `systemctl`
```
[admin@db ~]$ sudo systemctl start mariadb
[admin@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 15:31:12 CET; 2s ago
```

- exécutez la commande `sudo systemctl enable mariadb` pour faire en sorte que MariaDB se lance au démarrage de la machine
```
[admin@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```

- vérifiez qu'il est bien actif avec une commande `systemctl`
```
[admin@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 15:31:12 CET; 1min 38s ago
```

- déterminez sur quel port la base de données écoute avec une commande `ss`
```
[admin@db ~]$ sudo ss -ltnp
State        Recv-Q       Send-Q             Local Address:Port             Peer Address:Port      Process
LISTEN       0            128                      0.0.0.0:1032                  0.0.0.0:*          users:(("sshd",pid=1989,fd=4))
LISTEN       0            128                         [::]:1032                     [::]:*          users:(("sshd",pid=1989,fd=6))
LISTEN       0            80                             *:3306                        *:*          users:(("mysqld",pid=3403,fd=21))
```

- déterminez le(s) processus lié(s) au service MariaDB (commande `ps`)
```
[admin@db ~]$ sudo ps -ef | grep mysql
mysql       3403       1  0 15:31 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr
admin       3522    2128  0 15:37 pts/0    00:00:00 grep --color=auto mysql
```


🌞 **Firewall**

- pour autoriser les connexions qui viendront de la machine `web.tp2.cesi`, il faut conf le firewall
  - ouvrez le port utilisé par MySQL à l'aide d'une commande `firewall-cmd`
```
[admin@node1 ~]$ sudo firewall-cmd --permanent --add-port=3306/tcp
success
[admin@node1 ~]$ sudo firewall-cmd --reload
success
```

## 2. Conf MariaDB

🌞 **Configuration élémentaire de la base**

- exécutez la commande `mysql_secure_installation`
  - plusieurs questions successives vont vous être posées
  - prenez le temps de lire et de répondre de façon avisée avec la sécurité en tête
```
[admin@db ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n]
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```
---

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**


```bash
# Connexion à la base de données
[admin@db ~]$ sudo mysql -u root -p
[sudo] password for admin:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 30
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

Puis, dans l'invite de commande SQL :

```sql
# Création d'un utilisateur "nextcloud" dans la base, avec un mot de passe

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'miaou'
    -> ;
Query OK, 0 rows affected (0.001 sec)

# Création de la base de donnée qui sera utilisée par NextCloud

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

# On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
Query OK, 0 rows affected (0.001 sec)

# Actualisation des privilèges
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

## 3. Test

➜ **On va tester que la base sera utilisable par NextCloud.**


🌞 **Installez sur la machine `web.tp2.cesi` la commande `mysql`**
```
#Recherche du paquet
[admin@web ~]$ sudo dnf provides mysql
[sudo] password for admin:
Last metadata expiration check: 2:04:51 ago on Tue 07 Dec 2021 14:52:58 CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

#Install du paquet
[admin@web ~]$ sudo dnf install mysql
Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch               mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!
```

🌞 **Tester la connexion**

```
[admin@web ~]$ sudo mysql -h 10.2.1.12 -P 3306 -u nextcloud -p nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 36
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```


---

C'est bon ? Ca tourne ? **On part sur Apache maintenant !**

# II. Setup Apache


## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp2.cesi`**

```
[admin@web ~]$ sudo dnf install httpd
Installed:
  apr-1.6.3-12.el8.x86_64
  apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64
  apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!
```

---

🌞 **Analyse du service Apache**


```
#start et activation du service httpd (apache)
[admin@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:httpd.service(8)
[admin@web ~]$ sudo systemctl start httpd
[admin@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 17:41:35 CET; 1s ago
     Docs: man:httpd.service(8)
 Main PID: 3202 (httpd)
   Status: "Started, listening on: port 80"
    Tasks: 213 (limit: 4770)
   Memory: 25.7M
   CGroup: /system.slice/httpd.service
           ├─3202 /usr/sbin/httpd -DFOREGROUND
           ├─3203 /usr/sbin/httpd -DFOREGROUND
           ├─3204 /usr/sbin/httpd -DFOREGROUND
           ├─3205 /usr/sbin/httpd -DFOREGROUND
           └─3206 /usr/sbin/httpd -DFOREGROUND

Dec 07 17:41:34 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 07 17:41:35 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 07 17:41:35 web.tp2.cesi httpd[3202]: Server configured, listening on: port 80
[admin@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

#Processus et utilisateurs liés au service httpd
[admin@web ~]$ sudo ps -ef | grep httpd
root        3202       1  0 17:41 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      3203    3202  0 17:41 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      3204    3202  0 17:41 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      3205    3202  0 17:41 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      3206    3202  0 17:41 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
admin       3458    2272  0 17:45 pts/0    00:00:00 grep --color=auto httpd

#Port utilisé par Apache
[admin@web ~]$ sudo ss -ltnp
State      Recv-Q     Send-Q         Local Address:Port         Peer Address:Port    Process
LISTEN     0          128                  0.0.0.0:1032              0.0.0.0:*        users:(("sshd",pid=995,fd=5))
LISTEN     0          128                     [::]:1032                 [::]:*        users:(("sshd",pid=995,fd=7))
LISTEN     0          128                        *:80                      *:*        users:(("httpd",pid=3206,fd=4),("httpd",pid=3205,fd=4),("httpd",pid=3204,fd=4),("httpd",pid=3202,fd=4))
```


---

🌞 **Un premier test**

```
# ouverture du port d'Apache dans le firewall (fait lors du tp1)
[admin@node1 ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
success
[admin@node1 ~]$ sudo firewall-cmd --reload
success

# test connexion curl
  C:\Users\coral>curl 10.2.1.11
# connexion curl et web ok
```



### B. PHP


🌞 **Installer PHP**

```bash
# ajout des dépôts EPEL
[admin@web ~]$ sudo dnf install epel-release
Installed:
  epel-release-8-13.el8.noarch

Complete!
[admin@web ~]$ sudo dnf update
Upgraded:
  binutils-2.30-108.el8_5.1.x86_64                         bpftool-4.18.0-348.2.1.el8_5.x86_64
  kernel-tools-4.18.0-348.2.1.el8_5.x86_64                 kernel-tools-libs-4.18.0-348.2.1.el8_5.x86_64
  libgcc-8.5.0-4.el8_5.x86_64                              libgomp-8.5.0-4.el8_5.x86_64
  libipa_hbac-2.5.2-2.el8_5.1.x86_64                       libsss_autofs-2.5.2-2.el8_5.1.x86_64
  libsss_certmap-2.5.2-2.el8_5.1.x86_64                    libsss_idmap-2.5.2-2.el8_5.1.x86_64
  libsss_nss_idmap-2.5.2-2.el8_5.1.x86_64                  libsss_sudo-2.5.2-2.el8_5.1.x86_64
  libstdc++-8.5.0-4.el8_5.x86_64                           python3-perf-4.18.0-348.2.1.el8_5.x86_64
  python3-sssdconfig-2.5.2-2.el8_5.1.noarch                sssd-2.5.2-2.el8_5.1.x86_64
  sssd-ad-2.5.2-2.el8_5.1.x86_64                           sssd-client-2.5.2-2.el8_5.1.x86_64
  sssd-common-2.5.2-2.el8_5.1.x86_64                       sssd-common-pac-2.5.2-2.el8_5.1.x86_64
  sssd-ipa-2.5.2-2.el8_5.1.x86_64                          sssd-kcm-2.5.2-2.el8_5.1.x86_64
  sssd-krb5-2.5.2-2.el8_5.1.x86_64                         sssd-krb5-common-2.5.2-2.el8_5.1.x86_64
  sssd-ldap-2.5.2-2.el8_5.1.x86_64                         sssd-nfs-idmap-2.5.2-2.el8_5.1.x86_64
  sssd-proxy-2.5.2-2.el8_5.1.x86_64                        unzip-6.0-45.el8_4.x86_64
Installed:
  bind-libs-32:9.11.26-6.el8.x86_64                         bind-libs-lite-32:9.11.26-6.el8.x86_64
  bind-license-32:9.11.26-6.el8.noarch                      bind-utils-32:9.11.26-6.el8.x86_64
  fstrm-0.6.1-2.el8.x86_64                                  geolite2-city-20180605-1.el8.noarch
  geolite2-country-20180605-1.el8.noarch                    kernel-4.18.0-348.2.1.el8_5.x86_64
  kernel-core-4.18.0-348.2.1.el8_5.x86_64                   kernel-modules-4.18.0-348.2.1.el8_5.x86_64
  libmaxminddb-1.2.0-10.el8.x86_64                          protobuf-c-1.3.0-6.el8.x86_64
  python3-bind-32:9.11.26-6.el8.noarch                      python3-ply-3.9-9.el8.noarch

Complete!
# ajout des dépôts REMI
[admin@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[admin@web ~]$ dnf module enable php:remi-7.4


# install de PHP et de toutes les libs PHP requises par NextCloud
[admin@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Complete!
```

## 2. Conf Apache

---

🌞 **Analyser la conf Apache**

```
[admin@web httpd]$ sudo cat /etc/httpd/conf/httpd.conf
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```


🌞 **Créer un VirtualHost qui accueillera NextCloud**

```
# création du fichier de conf nextcloud
[admin@web conf.d]$ sudo vi nextcloud.conf
```

- ce fichier devra avoir le contenu suivant :

```apache
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/  

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

# redémarrage du service et check status
[admin@web conf]$ sudo systemctl restart httpd
[admin@web conf]$ sudo systemctl status httpd
```

> N'oubliez pas de redémarrer le service à chaque changement de la configuration, pour que les changements prennent effet.

🌞 **Configurer la racine web**

```
# création du chemin spécifié dans nextcloud.conf + droit pour l'utilisateur Apache
[admin@web /]$ cd /var/www/
[admin@web www]$ mkdir nextcloud
[admin@web www]$ sudo chown -R apache:apache nextcloud/

[admin@web ~]$ cd /var/www/nextcloud/
[admin@web nextcloud]$ sudo mkdir html
[admin@web nextcloud]$ sudo chown -R apache:apache html/
```

🌞 **Configurer PHP**

- dans l'install de NextCloud, PHP a besoin de conaître votre timezone (fuseau horaire)
```
[admin@web nextcloud]$ sudo cat /etc/opt/remi/php74/php.ini | grep -n date.timezone
922:; http://php.net/date.timezone
923:;date.timezone =
[admin@web nextcloud]$ sudo nano /etc/opt/remi/php74/php.ini
# utiliser ctrl+w pour rechercher le terme date.timezone
[admin@web nextcloud]$ sudo cat /etc/opt/remi/php74/php.ini | grep -n date.timezone
922:; http://php.net/date.timezone
923:;date.timezone = Europe/Paris (CET, +0100)
```

# III. NextCloud

On dit "installer NextCloud" mais en fait c'est juste récupérer les fichiers PHP, HTML, JS, etc... qui constituent NextCloud, et les mettre dans le dossier de la racine web.

🌞 **Récupérer Nextcloud**

```bash
[admin@web nextcloud]$ cd
[admin@web ~]$ sudo curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  4957k      0  0:00:30  0:00:30 --:--:-- 6550k
[admin@web ~]$ ls
nextcloud-21.0.1.zip
```

🌞 **Ranger la chambre**

```
# extraire le contenu du .zip
[admin@web ~]$ sudo unzip nextcloud-21.0.1.zip -d /var/www/nextcloud/html/

# déplacement du contenu du fichier nextcloud dans /html
[admin@web html]$ cd nextcloud/
[admin@web nextcloud]$ ls
3rdparty  config       core        index.php  ocm-provider  public.php  robots.txt  updater
apps      console.php  cron.php    lib        ocs           remote.php  status.php  version.php
AUTHORS   COPYING      index.html  occ        ocs-provider  resources   themes
[admin@web nextcloud]$ mv * /var/www/nextcloud/html/

# supprimer l'archive
[admin@web ~]$ sudo rm -r nextcloud-21.0.1.zip
```

## 4. Test

---

🌞 **Modifiez le fichier `hosts` de votre PC**

Rajout de la ligne en administrateur:
=> 10.2.1.11 web.tp2.cesi

🌞 **Tester l'accès à NextCloud et finaliser son install'**

- ouvrez votre navigateur Web sur votre PC
- rdv à l'URL `http://web.tp2.cesi`
- vous devriez avoir la page d'accueil de NextCloud
- ici deux choses :
  - les deux champs en haut pour créer un user admin au sein de NextCloud
  ```
  ID= nexcloud
  MDP = miaou
  ```

  
- entrez les infos pour que NextCloud sache comment se connecter à votre serveur de base de données
```
id = nextcloud
mdp = miaou
bdd = nextcloud
Connexion = 10.2.1.12:3306
```

  

---

**🔥🔥🔥 Baboom ! Un beau NextCloud.** => Il fonctionne !!!!


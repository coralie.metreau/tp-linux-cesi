# Partie 2 : Sécurisation

## Sommaire

- [Partie 2 : Sécurisation](#partie-2--sécurisation)
  - [Sommaire](#sommaire)
- [I. Serveur SSH](#i-serveur-ssh)
  - [1. Conf SSH](#1-conf-ssh)
  - [2. Bonus : Fail2Ban](#2-bonus--fail2ban)
- [II. Serveur Web](#ii-serveur-web)
  - [1. Reverse Proxy](#1-reverse-proxy)
  - [2. HTTPS](#2-https)

# I. Serveur SSH

## 1. Conf SSH

Si c'est pas fait vous **devez maintenant** configurer un accès par clés pour SSH.

🌞 **Modifier la conf du serveur SSH**

- désactiver la connexion de root
- désactiver la connexion par mot de passe et forcer la connexion par clés

```
# dans /etc/ssh/sshd_config faire les modifications suivantes:
PermitRootLogin no
PasswordAuthentication no
```
- forcer l'utilisation d'algorithmes de chiffrement forts
 ```
# dans /etc/ssh/sshd_config faire les modifications suivantes:
# algorithms
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
hostkeyalgorithms ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-ed25519,rsa-sha2-512,rsa-sha2-256
```


## 2. Bonus : Fail2Ban

Fail2Ban est un outil très simple :

- il lit les fichiers de logs en temps réel
- il repère des lignes en particulier, en fonction de patterns qu'on a défini
- si des lignes correspondant au même pattern sont répétées trop de fois, il effectue une action

Le cas typique :

- serveur SSH
- get attacked :(
- fail2ban détecte l'attaque rapidement, et ban l'IP source

Ca permet de se prémunir là encore d'attaques de masse : il faut que ça spam pour que fail2ban agisse. Des attaques qui essaient de bruteforce l'accès typiquement.

> C'est pas du tout que dans les séries Netflix hein. Vous mettez une machine avec une IP publique sur internet et c'est très rapide, vous vous faites attaquer toute la journée, tout le temps, plusieurs fois par minutes c'est pas choquant. Plus votre IP reste longtemps avec tel ou tel port ouvert, plus elle est "connue", et les attaques n'iront pas en diminuant.

🌞 **Installez et configurez fail2ban**

- je vous laisse chercher (en anglais sivoplé :( ) par vous-mêmes sur internet
  - [je vous link un random article plutôt ok qui explique le minimum pour SSH](https://www.techrepublic.com/article/how-to-install-fail2ban-on-rocky-linux-and-almalinux/)
- je vous impose pas une conf en particulier, le but c'est de savoir l'utiliser, au moins pour bloquer le spam sur votre serveur SSH
- **testez que ça fonctionne** : spammez un peu les connexions SSH (il faut que la connexion échoue : mauvais mot de passe par exemple), et faites-vous ban :)

> Le lien que j'ai envoyé ne parle pas en détail de comment ça marche. Simplement parce que SSH est très utilisé, et que fail2ban a déjà une pré-configuration pour fonctionner pour ce genre de services : il sait quelle ligne chercher et dans quel fichier. La doc de l'outil et/ou les fichiers de conf par défaut pourront vous renseigner +.

# II. Serveur Web

Bah ui certains l'ont fait en bonus hier, mais comme ça, on met tout le monde à niveau. Libre à vous de sauter ça ou de le refaire (un peu plus en conscience avec le cours sur TLS ?) ou simplement de le faire si c'était pas fait avant !

## 1. Reverse Proxy


---

🖥️ **Créez une nouvelle machine : `proxy.tp2.cesi`.** 🖥️

ip = 10.2.1.13


🌞 **Installer NGINX**

Nginx est déjà installé voir le tp1
```
[admin@proxy ~]$ sudo systemctl start nginx
[admin@proxy ~]$ sudo systemctl status nginx
```

🌞 **Configurer NGINX comme reverse proxy**

```
[admin@proxy ~]$ cd /etc/nginx/conf.d
[admin@proxy conf.d]$ vi proxy.conf
[admin@proxy conf.d]$ sudo vi proxy.conf

#Mettre ces informations:
server {
    listen 80;
    server_name web.tp2.cesi;

    location / {
       proxy_pass http://10.2.1.11;
    proxy_http_version  1.1;
    proxy_cache_bypass  $http_upgrade;

    proxy_set_header Upgrade           $http_upgrade;
    proxy_set_header Connection        "upgrade";
    proxy_set_header Host              $host;
    proxy_set_header X-Real-IP         $remote_addr;
    proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Host  $host;
    proxy_set_header X-Forwarded-Port  $server_port;
    }
}

[admin@proxy conf.d]$ sudo systemctl restart nginx

```


🌞 **Une fois en place, text !**

```
# modification du fichier host de la machine physique
10.2.1.13 web.tp2.cesi
```
=> connexion ok

## 2. HTTPS

🌞 **Générer une clé et un certificat avec la commande suivante :**

```bash
[admin@proxy ~]$ openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt
Generating a RSA private key
............................++++
...................++++
writing new private key to 'server.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:France
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:cesi
Organizational Unit Name (eg, section) []:it
Common Name (eg, your name or your server's hostname) []:web.tp2.cesi
Email Address []:contact@cesi.fr
[admin@proxy ~]$ ls
server.crt  server.key
```

🌞 **Allez, faut ranger la chambre**

```
# déplacement de la clé et du certificat
[admin@proxy ~]$ sudo mv server.key /etc/pki/tls/private/
[sudo] password for admin:
[admin@proxy ~]$ ls /etc/pki/tls/private/
server.key
[admin@proxy ~]$ sudo mv server.crt /etc/pki/tls/cert
cert.pem  certs/
[admin@proxy ~]$ sudo mv server.crt /etc/pki/tls/cert
cert.pem  certs/
[admin@proxy ~]$ sudo mv server.crt /etc/pki/tls/certs
[admin@proxy ~]$ ls /etc/pki/tls/certs
ca-bundle.crt  ca-bundle.trust.crt  server.crt

#renommer la clé et le certificat
[admin@proxy private]$ sudo mv server.key web.tp2.cesi.key
[admin@proxy certs]$ sudo mv server.crt web.tp2.cesi.crt
```


🌞 **Affiner la conf de NGINX**


```nginx
# cette ligne 'listen', vous l'avez déjà. Remplacez-la.
listen                  443 ssl http2;

# nouvelles lignes
# remplacez les chemins par la clé et le cert que vous venez de générer
    ssl_certificate /etc/pki/tls/certs/web.tp2.cesi.crt
    ssl_certificate /etc/pki/tls/private/web.tp2.cesi.key
```


🌞 **Test !**

curl -k https://web.tp2.cesi -L
=> connexion ok en https

🌞 **Bonus**

- **ajouter le certificat au magasin de certificat de votre navigateur** pour avoir un pitit cadenas vert
  - très utilisé en entreprise, avec des méthodes automatisées bien sûr : déployer un cert interne sur les postes client pour trust les applications internes
- **ajouter une redirection HTTP -> HTTPS**
  - c'est très courant aussi : on fait en sorte que si un client arrive en HTTP, il soit redirigé vers le site en HTTPS
  - ça permet aux clients qui arrivent en HTTP de pas manger un gros "CONNECTION REFUSED" dans leur navigateur :)
    - [hop un artiiiicle qui parle de ça](https://linuxize.com/post/redirect-http-to-https-in-nginx/) (n'hésitez pas, comme toujours, à chercher par vous-mêmes)
- **renforcer l'échange TLS en sélectionnant les algos de chiffrement à utiliser**
  - cherchez "nginx strong ciphers" :)
- **répartition de charge**
  - clonez la VM Apache + NextCloud
  - mettez en place de la répartition de charge entre les deux machines renommées `web1.tp2.cesi` et `web2 .tp2.cesi`

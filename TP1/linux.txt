TP 1 Linux:
Première partie:

Nom pc physique = PC

Paramètre VM
NOM = "rocky"
OS = Rocky linux
Ram = 1Go
Ajout d'une seconde carte réseau VMNET1

Configuration IP
$ip a
$cd /etc/sysconfig/network-scripts
$cp ifcfg-ens160 ifcfg-ens224
$vi ifcfg-ens224

$sudo nmcli con reload
$sudo nmcli con up ens224
$ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens160: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:0c:29:99:a6:bd brd ff:ff:ff:ff:ff:ff
    inet 192.168.79.133/24 brd 192.168.79.255 scope global dynamic noprefixroute ens160
       valid_lft 940sec preferred_lft 940sec
    inet6 fe80::20c:29ff:fe99:a6bd/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: ens224: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:0c:29:99:a6:c7 brd ff:ff:ff:ff:ff:ff
    inet 10.250.1.2/24 brd 10.1.1.255 scope global noprefixroute ens224
       valid_lft forever preferred_lft forever
    inet6 fe80::20c:29ff:fe99:a6c7/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

Modification carte réseau PC
=> modification ip carte vmnet01 en 10.250.1.1/24
=> modification dans vmware
Edit => Virtual Network Editor => Change settings => Vmnet1 en subnet 10.1.1.0

Ping ok entre les deux machines
PC => Rocky
C:\Users\coral>ping 10.250.1.2

Envoi d’une requête 'Ping'  10.250.1.2 avec 32 octets de données :
Réponse de 10.250.1.2 : octets=32 temps<1ms TTL=64
Réponse de 10.250.1.2 : octets=32 temps<1ms TTL=64
Réponse de 10.250.1.2 : octets=32 temps<1ms TTL=64
Réponse de 10.250.1.2 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 10.250.1.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms


Rocky => PC
Rajout sur ma machine de la commande en admin: netsh advfirewall firewall add rule name=ping proto="icmpv4:8,any" dir=in action=allow
[admin@localhost ~]$ ping 10.250.1.1
PING 10.250.1.1 (10.250.1.1) 56(84) bytes of data.
64 bytes from 10.250.1.1: icmp_seq=1 ttl=128 time=0.118 ms
64 bytes from 10.250.1.1: icmp_seq=2 ttl=128 time=0.215 ms
^C
--- 10.250.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1011ms
rtt min/avg/max/mdev = 0.118/0.166/0.215/0.050 ms

DNS
[admin@localhost ~]$ cat /etc/sysconfig/network-scripts/ifcfg-ens224
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=ens224
DEVICE=ens224
ONBOOT=yes
IPADDR=10.250.1.2
NETMASK=255.255.255.0
DNS1=1.1.1.1

$sudo nmcli con reload
$sudo nmcli con up ens224

Ping google.com
[admin@localhost ~]$ ping google.com
PING google.com (142.250.74.238) 56(84) bytes of data.
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=128 time=41.9 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=128 time=18.2 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 18.154/30.005/41.857/11.852 ms

HOSTNAME
[admin@localhost ~]$ sudo hostname node1.tp1.cesi
[sudo] password for admin:
[admin@localhost ~]$ hostname
node1.tp1.cesi

USER
[admin@localhost etc]$ sudo cat /etc/sudoers | grep wheel
## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL
# %wheel        ALL=(ALL)       NOPASSWD: ALL

$sudo useradd Coco -d /home/coco -G wheel
$sudo passwd Coco

SSH
Ouverture powershell
ssh admin@10.250.1.2

SELinux
$cd /etc/selinux/
$sudo sestatus
$sudo setenforce 0
$vi config
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=permissive
# SELINUXTYPE= can take one of these three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted
$sudo sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   permissive
Mode from config file:          permissive
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33

Seconde partie:

SSH

$sudo systemctl status sshd
il est actif
[admin@localhost selinux]$ sudo ss -lutpn
Netid   State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port   Process
tcp     LISTEN    0         128                0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=1016,fd=5))
tcp     LISTEN    0         128                   [::]:22                 [::]:*       users:(("sshd",pid=1016,fd=7))

[admin@localhost selinux]$ ps -ef | grep ssh
root        2556    2496  0 15:25 ?        00:00:00 sshd: admin [priv]
admin       2560    2556  0 15:25 ?        00:00:00 sshd: admin@pts/0

[admin@localhost selinux]$ cd /etc/ssh/
[admin@localhost ssh]$ sudo vi sshd_config
[admin@localhost ssh]$ sudo vi sshd_config
port 42069
[admin@localhost ssh]$ sudo systemctl restart sshd
[admin@localhost ssh]$ sudo firewall-cmd --permanent --list-all
public
  target: default
  icmp-block-inversion: no
  interfaces:
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[admin@localhost ssh]$ sudo systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-12-06 15:10:53 CET; 8s ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 2496 (sshd)
    Tasks: 1 (limit: 4770)
   Memory: 1.1M
   CGroup: /system.slice/sshd.service
           └─2496 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cb>
Dec 06 15:10:53 node1.tp1.cesi systemd[1]: sshd.service: Succeeded.
Dec 06 15:10:53 node1.tp1.cesi systemd[1]: Stopped OpenSSH server daemon.
Dec 06 15:10:53 node1.tp1.cesi systemd[1]: Starting OpenSSH server daemon...
Dec 06 15:10:53 node1.tp1.cesi sshd[2496]: Server listening on 0.0.0.0 port 1032.
Dec 06 15:10:53 node1.tp1.cesi sshd[2496]: Server listening on :: port 1032.
Dec 06 15:10:53 node1.tp1.cesi systemd[1]: Started OpenSSH server daemon.
PS C:\Users\coral> ssh admin@10.250.1.2 -p 1032
admin@10.250.1.2's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Dec  6 13:52:36 2021 from 10.250.1.1

SERVICE WEB
[admin@node1 ~]$ sudo dnf install nginx
[admin@node1 ~]$ sudo systemctl start nginx
[admin@node1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-06 15:29:03 CET; 5s ago
  Process: 26747 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 26745 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 26744 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 26749 (nginx)
    Tasks: 2 (limit: 4770)
   Memory: 4.7M
   CGroup: /system.slice/nginx.service
           ├─26749 nginx: master process /usr/sbin/nginx
           └─26750 nginx: worker process

Dec 06 15:29:03 node1.tp1.cesi systemd[1]: Starting The nginx HTTP and reverse proxy server...
Dec 06 15:29:03 node1.tp1.cesi nginx[26745]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Dec 06 15:29:03 node1.tp1.cesi nginx[26745]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Dec 06 15:29:03 node1.tp1.cesi systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid argument
Dec 06 15:29:03 node1.tp1.cesi systemd[1]: Started The nginx HTTP and reverse proxy server.
[admin@node1 ~]$ sudo ss -ltnp
State  Recv-Q Send-Q  Local Address:Port   Peer Address:Port Process
LISTEN 0      128           0.0.0.0:1032        0.0.0.0:*     users:(("sshd",pid=2496,fd=4))
LISTEN 0      128           0.0.0.0:80          0.0.0.0:*     users:(("nginx",pid=26750,fd=8),("nginx",pid=26749,fd=8))
LISTEN 0      128              [::]:1032           [::]:*     users:(("sshd",pid=2496,fd=6))
LISTEN 0      128              [::]:80             [::]:*     users:(("nginx",pid=26750,fd=9),("nginx",pid=26749,fd=9))
[admin@node1 ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
success
[admin@node1 ~]$ sudo firewall-cmd --reload
success
Connexion via le web ok 
via cmd 
C:\Windows\system32>curl 10.250.1.2
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

VOTRE PROPRE SERVICE
[admin@node1 ~]$ sudo dnf install python3
[admin@node1 ~]$ sudo firewall-cmd --permanent --add-port=8888/tcp
success
[admin@node1 ~]$ sudo firewall-cmd --reload
success
[admin@node1 system]$ sudo nano service.service
[sudo] password for admin:
[admin@node1 system]$ [admin@node1 system]$
[admin@node1 system]$ cat nano .service
cat: nano: No such file or directory
[Unit]
Description=Start Python3 WebServer on 8888

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
[admin@node1 system]$ sudo systemctl daemon-reload
[admin@node1 system]$ sudo systemctl start service.service
[admin@node1 system]$ sudo systemctl status service.service
● service.service - Start Python3 WebServer on 8888
   Loaded: loaded (/etc/systemd/system/service.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-06 16:03:52 CET; 1s ago
 Main PID: 27322 (python3)
    Tasks: 1 (limit: 4770)
   Memory: 9.4M
   CGroup: /system.slice/service.service
           └─27322 /bin/python3 -m http.server 8888

Dec 06 16:03:52 node1.tp1.cesi systemd[1]: Started Start Python3 WebServer on 8888.
[admin@node1 system]$ sudo systemctl enable service.service
[admin@node1 system]$ useradd web
[admin@node1 system]$ cd /srv
[admin@node1 srv]$ sudo mkdir web
[admin@node1 srv]$ ls
web
[admin@node1 srv]$ cd web/
[admin@node1 web]$ sudo vi page.txt
[admin@node1 /]$ sudo chown web /srv/web
[admin@node1 srv]$ ls -al
total 0
drwxr-xr-x.  3 root root  17 Dec  6 16:12 .
dr-xr-xr-x. 17 root root 224 Dec  6 11:18 ..
drwxr-xr-x.  2 web  root  22 Dec  6 16:21 web
[admin@node1 srv]$ sudo vi /etc/systemd/system/service.service
[Unit]
Description=Start Python3 WebServer on 8888

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv/web
[Install]
WantedBy=multi-user.target

[admin@node1 srv]$ sudo systemctl daemon-reload
[admin@node1 srv]$ sudo systemctl restart service.service
[admin@node1 srv]$ sudo systemctl status service.service
● service.service - Start Python3 WebServer on 8888
   Loaded: loaded (/etc/systemd/system/service.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-06 16:33:31 CET; 9s ago
 Main PID: 27596 (python3)
    Tasks: 1 (limit: 4770)
   Memory: 9.4M
   CGroup: /system.slice/service.service
           └─27596 /bin/python3 -m http.server 8888

Dec 06 16:33:31 node1.tp1.cesi systemd[1]: service.service: Succeeded.
Dec 06 16:33:31 node1.tp1.cesi systemd[1]: Stopped Start Python3 WebServer on 8888.
Dec 06 16:33:31 node1.tp1.cesi systemd[1]: Started Start Python3 WebServer on 8888.

# TP3 : Docker


- [TP3 : Docker](#tp3--docker)
- [Prérequis](#prérequis)
- [I. Setup](#i-setup)
- [II. Premiers pas](#ii-premiers-pas)
  - [1. Conteneur NGINX](#1-conteneur-nginx)
  - [2. Une vraie application](#2-une-vraie-application)
- [III. Dockerfile](#iii-dockerfile)
- [IV. docker-compose](#iv-docker-compose)
  - [1. Premiers pas](#1-premiers-pas)
  - [2. NextCloud](#2-nextcloud)

# Prérequis

ip = 10.2.1.14

# I. Setup

🌞 **Installez Docker en suivant [la doc officielle](https://docs.docker.com/)**

> Vous pouvez suivre, pour Rocky, les instructions données pour CentOS.

```
[admin@docker ~]$ sudo yum remove docker 
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine

[admin@docker ~]$ sudo dnf update -y
Complete!
```

🌞 **Setup Docker**



- ajoutez votre utilisateur au groupe `docker`
 - il faudra quitter puis réouvrir une session pour que cela prenne effet
```
[admin@docker ~]$ sudo usermod -aG docker admin
[sudo] password for admin:
[admin@docker ~]$ groups admin
admin : admin wheel docker
```
 
- démarrez le service `docker`
- activez le service `docker` au démarrage de la machine
- vérifiez avec un `docker info` que tout est ok
  - vous devriez bah avoir une réponse et pas une erreur

```
[admin@docker ~]$ sudo systemctl start docker
[admin@docker ~]$ sudo systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
[admin@docker ~]$ sudo systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-12-09 15:58:30 CET; 14s ago
```

# II. Premiers pas

## 1. Conteneur NGINX

🌞 **Lancez un conteneur NGINX**

- utilisez l'image NGINX officielle
```
[admin@docker ~]$ sudo docker pull nginx
Using default tag: latest
latest: Pulling from library/nginx
e5ae68f74026: Pull complete
21e0df283cd6: Pull complete
ed835de16acd: Pull complete
881ff011f1c9: Pull complete
77700c52c969: Pull complete
44be98c0fab6: Pull complete
Digest: sha256:9522864dd661dcadfd9958f9e0de192a1fdda2c162a35668ab6ac42b465f0603
Status: Downloaded newer image for nginx:latest
docker.io/library/nginx:latest
```


- partagez le port 8080 de l'hôte vers le port 80 du conteneur
- le conteneur doit être lancé en *daemon*
- NGINX doit servir une page HTML que vous avez créée vous-mêmes
  - et, pas de miracles hein, juste un `coucou` dans un fichier `index.html` ça ira
  - la racine web doit être `/var/www/html/cesi/`
  - on aura donc un fichier `/var/www/html/cesi/index.html`
- vous prenez pas la tête, partez d'un fichier de conf minimal :

```nginx
events {
    multi_accept       on;
    worker_connections 65535;
}

http {
  server {
    listen 80;

    server_name web.tp3.cesi;

    location / {
      root <PATH_TO_WEBROOT>;
      index index.html;
    }
  }
}
```

> Vous allez devoir utiliser un `-v` sur la ligne de commande `docker run` pour monter un fichier dans le conteneur (en l'occurence : deux `-v`, un pour la conf et un pour l'index HTML).

🌞 **Vous devez accéder à votre page HTML, depuis votre navigateur en tapant `http://web.tp3.cesi`**

🌞 **Manipuler le conteneur qui tourne**

- donnez les commandes que vous utilisez pour récupérer un terminal dans le conteneur
- déterminer l'adresse IP locale du conteneur avec une commande `docker inspect`
- relancer un nouveau conteneur en limitant son utilisation RAM à 128M
  - c'est une option de la commande `docker run`
  - `docker run --help` et `man docker run`
  - prouvez qu'il est bien limité en RAM avec une commande `docker stats`

## 2. Une vraie application

🌞 **Créer un réseau docker**

- avec une commande `docker network create <NAME>`
- vous l'appelerez `wiki`
- vous pouvez le voir et l'inspecter (`docker network ls` et `docker network inspect <NETWORK>`)

🌞 **Lancez un conteneur MySQL**

- il devra :
  - les variables d'environnement servent pour la conf initiale :
    - un mot de passe root défini
    - un user créé
    - un mot de passe pour ce user
    - une base de données `wiki`
  - être dans le réseau `wiki`
  - être lancé en *daemon*
- assurez vous qu'il est fonctionnel en vous connectant à la base
  - d'abord récupérez un shell dans le conteur
  - puis une connexion locale à la base avec une commande `mysql`
- **avoir un nom** avec `--name`

> Il existe une image officielle MySQL. Le README du Hub est clair, il vous donnera la ligne `docker run` et les variables d'environnement à passer à la base.  
Il faut utiliser l'option `-e` de `docker run` pour passer des variables d'environnement au conteneur, vous verrez des exemples dans le README de MySQL sur le Hub ;)

🌞 **Lancez un conteneur [WikiJS](https://js.wiki/)**

- il devra :
  - être accessible sur un port de l'hôte
  - être dans le réseau `wiki`
  - utiliser la base lancée précédemment
  - être exposé sur l'hôte grâce à un partage de port
- tester que vous pouvez accéder à l'application depuis votre navigateur

> Y'a tout dans la doc officielle, la p'tite ligne `docker run` qui va bien.

# III. Dockerfile

Un `Dockerfile` est un fichier qui sert à construire une nouvelle image. Une fois le Dockerfile créé, on peut utilser la commande `docker build` afin de créer une image.

Le fichier est vraiment nommé comme ça, avec un majuscule, par convention.

> Voir le [mémo Docker](../../cours/memo/docker.md) pour un exemple de `Dockerfile` et de `docker build`.

🌞 **Créez une image**

- elle doit faire tourner notre ami `python3 -m http.server 8888`
  - pour rappel, ça lance un mini serveur web à l'aide de Python
- vous devez partir de [l'image Debian officielle](https://hub.docker.com/_/debian)
- le serveur Python doit rendre accessible le contenu de `/srv/test`
- le dossier `/srv/test/` doit contenir un fichier `index.html`

🌞 **Test**

- testez que vous accédez à l'application, depuis votre navigateur

# IV. docker-compose

## 1. Premiers pas

🌞 **Installez `docker-compose`** en suivant la doc officielle

> Là encore, vous pouvez suivre, sur Rocky, les instruction données pour CentOS.

---

🌞 **Lancez un `docker-compose.yml` de test**

- la commande c'est `docker-compose up`
  - la commande ne peut être lancée que s'il y a un `docker-compose.yml dans le dossier courant
- contenu du fichier `docker-compose.yml` :

```yml
version: "3.5"

services:
  web:
    image: nginx
    ports: 
      - "8080:80"
    networks:
      test-net:

networks:
  test-net:
```

Créez un nouveau dossier, dans votre homedir par exemple, et créez un nouveau fichier `docker-compose.yml` pour y déposer le contenu ci-dessus. Ce sera votre répertoire de travail.  

**Vous n'aurez accès aux commandes `docker-compose` que si vous vous trouvez dans un répertoire qui contient un fichier `docker-compose.yml`.**  

> Il en va de la bonne pratique de dédier entièrement ce dossier à l'application mise en place par le fichier `docker-compose.yml`. Et donc, de ne stocker d'autres fichiers que s'ils sont nécessaires au bon fonctionnement de l'application.

🌞 **Vérifier**

- que ça tourne avec un `docker ps`
- `docker-compose ps` est aussi dispo
- visitez le site depuis votre navigateur

---

Manipulons un peu le fichier `docker-compose.yml` ! 

🌞 **Dans le même fichier**

- ajouter un conteneur debian
- il doit lancer la commande `sleep 99999` au démarrage
- il doit être dans le même réseau

🌞 **Lancez le `docker-compose.yml` modifié**

- récupérez un shell dans le conteneur debian
- vérifiez que vous pouvez ping l'autre conteneur en utilisant son nom (pas son IP) : `ping <NOM>`

---

L'utilisation de variables permettra d'éviter de répéter plusieurs fois la même valeur dans notre `docker-compose.yml`, en plus d'en faciliter la modification.

🌞 **Variables**

- créez dans le même dossier que le fichier `docker-compose.yml` un fichier `.env`
  - à l'intérieur, vous pouvez définir des variables sous la forme `VAR=VALEUR`
  - ces variables sont accessibles dans le `docker-compose.yml`
- définissez une variable `NGINX_PORT` à la valeur `8080`
- utilisez la variable dans le fichier pour exposer le port de NGINX

## 2. NextCloud

🌞 **Créez un `docker-compose.yml`**

- il lance 3 conteneurs
  - un conteneur NextCloud
    - il existe un conteneur édité par NextCloud sur le hub
  - un conteneur MySQL
    - image officielle
  - un conteneur NGINX
    - image officielle
- le conteneur NGINX
  - doit proxy vers NextCloud
  - doit permettre l'établissement de connexions HTTPS
  - doit être exposé via un partage de port sur l'hôte
- l'application doit être joignable sur le nom `nextcloud.tp3.cesi`
- comme dans l'exemple un peu plus bas, le fichier utilise un maximum de variable

> Allez-y étape par étape. Lancez déjà NextCloud et sa base de données avec `docker-compose`. Ajoutez le proxying. Puis enfin le proxying en HTTPS.  
N'oubliez pas que les conteneurs se joignent naturellement *via* leurs noms s'ils sont dans le même réseau.

Créez un répertoire dédié à travailler sur ce `docker-compose.yml`. Le mien ressemblait à ça à la fin :

```bash
.
├── data/                   # monté dans le conteneur NextCloud
├── db/                     # monté dans le conteneur MySQl
├── docker-compose.yml
└── nginx/
    ├── nginx.conf          # monté dans le conteneur NGINX
    ├── web.tp3.cesi.crt    # monté dans le conteneur NGINX
    └── web.tp3.cesi.key    # monté dans le conteneur NGINX
```

Il est possible de donner plusieurs noms aux conteneurs avec cette syntaxe (les `aliases`) :

```bash
nextcloud_app:
    image: nextcloud
    restart: always
    volumes:
      - ./data:/var/www/html
    environment:
      - MYSQL_HOST=nextcloud_db
      - MYSQL_DATABASE=$NEXTCLOUD_MYSQL_DATABASE
      - MYSQL_USER=$NEXTCLOUD_MYSQL_USER
      - MYSQL_PASSWORD=$NEXTCLOUD_MYSQL_PASSWORD
    networks:
      nextcloud:
        aliases:
          - web.tp3.cesi
```

> Pour le détail de la génération des certificats ou de la configuration du proxy, référez-vous à [la partie 2 du TP2](../2/part2.md#1-reverse-proxy)

---

GG :)

![I Know Docker](./pics/i_know_docker.jpg)
